import requests
import json
from transformers import pipeline, set_seed
import os
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import logging
logging.basicConfig(level='ERROR')

cookies_1 = {"_ga": "GA1.2.322968960.1654086350",
             "_fbp": "fb.1.1654086350025.1812274045",
             "intercom-id-lke45jp1": "8eb2d8b0-7503-4deb-8bd4-ddd3bbb675b7",
             "intercom-session-lke45jp1": "",
             "__stripe_mid": "60261877-8b11-464a-9f21-782e3c072c5324fb31",
             "_gid": "GA1.2.42386260.1654524714",
             "PHPSESSID": "7248a4ab34d22fc5b5ecade261d1e715",
             "_id": "ef696e0d4563237bd5c59a64c03fff6ddbfa9a032490378ac4d09cd60ccb068da%3A2%3A%7Bi%3A0%3Bs%3A3%3A%22_id%22%3Bi%3A1%3Bs%3A33%3A%22%5B37605%2C%22not+implemented%22%2C2592000%5D%22%3B%7D",
             "_csrf": "a3e46ac43c0abb98c3f9b3821be6dd4a176614f0434cb713e5e472449258ce35a%3A2%3A%7Bi%3A0%3Bs%3A5%3A%22_csrf%22%3Bi%3A1%3Bs%3A32%3A%22NH-LF8JV2ay8x7lhzm03maRLpb5Gzo1_%22%3B%7D"}


def title_generator(input_str):
    url = "https://tuberanker.com/related-titles"
    payload = json.dumps({
        "keywords": input_str
    })
    headers = {
        'Content-Type': 'application/json',
    }

    response = requests.request("POST", url, headers=headers, data=payload, cookies=cookies_1)
    return response.json()['data']['person']


def tag_generator(keyword):
    url = "https://tuberanker.com/complete-search"
    payload = json.dumps({
        "keyword": keyword
    })
    headers = {
        'Content-Type': 'application/json',
    }

    response = requests.request("POST", url, headers=headers, data=payload, cookies=cookies_1)
    return response.json()['data']


def description_generator(input_str):
    generator = pipeline('text-generation', model='gpt2')
    set_seed(42)
    text_generates = generator(input_str, max_length=150, num_return_sequences=5)
    results = []
    for i in text_generates:
        results.append(i['generated_text'])

    return results




