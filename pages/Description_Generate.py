import streamlit as st
from services.generator import *

input_description = st.text_input('Input something for generate description')
if input_description:
    descriptions = description_generator(input_description)
    for i in range(len(descriptions)):
        st.write("Description number" + str(i))
        st.write(descriptions[i] + '\n\n')