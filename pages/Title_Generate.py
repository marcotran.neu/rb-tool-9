import streamlit as st
from services.generator import *

title = st.text_input('Input something for generate title')
if title:
    title_generators = title_generator(title)
    for title_generated in title_generators:
        st.write(title_generated)