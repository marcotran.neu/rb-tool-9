import streamlit as st
from services.generator import *

tag_keyworks = st.text_input('Input something for generate tags')
if tag_keyworks:
    tag_generators = tag_generator(tag_keyworks)
    for tag in tag_generators:
        st.write(tag)